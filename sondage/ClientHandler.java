package sondage;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientHandler implements Runnable {
    private Socket socket;
    private Sondage sondage;
    private PrintWriter writer;
    private BufferedReader reader;
    
    public ClientHandler(Socket socket, Sondage sondage) {
        this.socket = socket;
        this.sondage = sondage;

        try {
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                String[] msg = reader.readLine().split(" ");
                // System.out.println(msg[0] + ", " + msg[1]);
                switch (msg[0]) {
                    case "vote":
                        try {
                            if (this.sondage.voter(msg[1]) == true){
                                writer.println("OK");
                                writer.flush();
                            } else {
                                writer.println("ERR");
                                writer.flush();
                            }
                        } catch (Exception e) {
                           e.printStackTrace();
                        }
                        break;

                    case "candidats":
                        writer.println(sondage.toString());
                        writer.flush();
                        System.out.println("yesss");
                        break;

                    case "resultat":
                        writer.println("max : test");
                        writer.flush();
                        break;
                        
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    
}
