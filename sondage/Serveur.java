package sondage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {
    public static void main(String[] args) {
        try {
            ServerSocket serveurSocket = new ServerSocket(5555);
            Sondage sondage = new Sondage(1);
            sondage.getHashMap().put("Joe", 0);
            sondage.getHashMap().put("Doe", 0);
            sondage.getHashMap().put("Ré", 0);
            sondage.getHashMap().put("Mi", 0);

            while (true) {
                Socket socketClient = serveurSocket.accept();
                Thread clientHandler = new Thread(new ClientHandler(socketClient, sondage));
                clientHandler.start();
                System.out.println("ok");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}