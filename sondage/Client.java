package sondage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class Client {
    private int idClient;
    private List<Sondage> lesSondages;

    public Client(int idClient, List<Sondage> lesSondages) {
        this.idClient = idClient;
        this.lesSondages = lesSondages;
    }

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 5555);
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));


            while (socket.isConnected()) {
                // Thread recevoirThread = new Thread(new Runnable() {

                //     @Override
                //     public void run() {
                //         try {
                //             String msgRecu = reader.readLine();
                //             if (msgRecu != null) {
                //                 System.out.println("Serveur : " + msgRecu);
                //             }
                //         } catch (IOException e) {
                //             e.printStackTrace();
                //         }
                //     }
                // });

                // recevoirThread.start();
                
                Scanner scanner = new Scanner(System.in);

                String msgEnvoie = scanner.nextLine();
                writer.println(msgEnvoie);
                writer.flush();

                String msgRecu = reader.readLine();
                if (msgRecu != null) {
                    System.out.println("Serveur : " + msgRecu);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
