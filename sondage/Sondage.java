package sondage;

import java.util.HashMap;

public class Sondage {
    private HashMap<String, Integer> lesPropositions; // La proposition / Nb de votes
    private int numSondage;

    public Sondage(int numSondage) {
        this.lesPropositions = new HashMap<String, Integer>();
        this.numSondage = numSondage;
    }

    public int getNumSondage() {
        return this.numSondage;
    }

    public synchronized boolean voter(String numeroProposition) {
            if (this.lesPropositions.containsKey(numeroProposition)) {
                lesPropositions.put(numeroProposition, lesPropositions.get(numeroProposition) + 1);
                return true;
            }
            return false;
    }

    public int getNbDeVotes() {
        int res = 0;
        for (int votesDeLaProosition : lesPropositions.values()) {
            res += votesDeLaProosition;
        }
        return res;
    }

    public HashMap<String, Integer> getHashMap() {
        return this.lesPropositions;
    }

    public String toString() {
        return "{numéro sondage : " + numSondage + ", nombre de vote(s) : " + getNbDeVotes() + ", propositions : " + lesPropositions.keySet() +  "}";
    }
}
