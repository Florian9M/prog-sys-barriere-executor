import java.util.concurrent.ExecutionException;

public class MainMaxFinder {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Tab tab = new Tab();
        tab.updateMax();
        System.out.println(tab.getMax());
    }
}
