import java.util.concurrent.RecursiveTask;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class MaxFinder extends RecursiveTask<Integer> {
    private int debut, fin;
    private Tab tab;
    private final int seuil = 10;
    private Tab liste;

    public MaxFinder(int debut, int fin, Tab liste) {
        this.debut = debut;
        this.fin = fin;
        this.liste = liste;
    }

    public Integer compute() {
        if (fin - debut < seuil) {
            int max = Integer.MIN_VALUE;
            for (int i = debut; i < fin; i++) {
                max = Math.max(max, liste.getListe().get(i));
            }
            return max;
        } else {
            int millieu = (fin - debut) / 2 + debut;
            MaxFinder left = new MaxFinder(debut, millieu, liste);
            left.fork();
            MaxFinder right = new MaxFinder(millieu, fin, liste);
            right.fork();
            return Math.max(right.join(), left.join());
        }
    }
}
