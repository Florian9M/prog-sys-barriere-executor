import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

public class Tab {
    private List<Integer> liste;
    private int max;

    public Tab() {
        this.liste = Arrays.asList(1, 2, 21, 231, 312, 0, -2, 7, 2, 2321, 84, 21, 21);
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return this.max;
    }

    public void updateMax() throws InterruptedException, ExecutionException {
        ForkJoinPool pool = new ForkJoinPool();
        Future f = pool.submit(new MaxFinder(0, liste.size(), this));
        int res = (int) f.get(); 
        setMax(res);
    }

    public List<Integer> getListe() {
        return this.liste;
    }
}
