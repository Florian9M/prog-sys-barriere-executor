import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Infos implements Runnable {
    CyclicBarrier barriere;
    private int id;
    
    public Infos(int id, CyclicBarrier barriere) {
        this.id = id;
        this.barriere = barriere;
    }

    public int getId() {
        return this.id;
    }

    @Override
    public void run() {
        System.out.println(String.format("Infos : %d", this.id));
        try {
            System.out.println("debut attente : " + this.id);
            barriere.await();
            System.out.println("fin attente : " + this.id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}