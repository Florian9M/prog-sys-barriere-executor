import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Exec {
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException, ExecutionException {
        ExecutorService pool = Executors.newFixedThreadPool(5);
        CyclicBarrier barriere = new CyclicBarrier(5);
        List<Future> l = new ArrayList<>();

        long start = System.currentTimeMillis();

        for (int i = 0; i < 10; i ++) {
            Future<?> f = pool.submit(new Infos(i, barriere));
            l.add(f);
        }

        for (Future f : l) {
            f.get();
        }

        pool.shutdown();

        long end = System.currentTimeMillis();
        long elapsedTime = end - start;
        System.out.println(elapsedTime);

        System.out.println("avant shutdown");
        System.out.println("shutdown");
                
        pool.awaitTermination(0,TimeUnit.MILLISECONDS);
    }
}
